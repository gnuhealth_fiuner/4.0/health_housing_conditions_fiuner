from decimal import Decimal

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, And, Greater, Or
from trytond import backend
from trytond.transaction import Transaction


class HousingConditions(ModelSQL,ModelView):
    'DU Housing conditions'
    __name__ = 'gnuhealth.housing'

    du = fields.Many2One('gnuhealth.du','DU',required=True,
        help="Unidad domiciliaria")
    revision_date = fields.Date(u'Fecha de revisión',
        help = u'Fecha a la cuál se midio\n o se tiene '
        'certeza de\n las condiciones del hogar',
        required = True)
    housing_type = fields.Selection([
        (None,''),
        ('sin_dato','Sin dato'),
        ('1', '   1   ->  Vivienda de material, terminada y en buenas condiciones'),
        ('2', '   2   ->  Vivienda sin terminar o con faltante de aberturas'),
        ('3', '   3   ->  Vivienda precaria (Ej: rancho o casilla)'),
        ('another','Other'),
        ],'Clasificacion',help = 'Clasificacion segun planilla de Censo'
        , sort=False, required=True)
    housing_type_another = fields.Char('Housing type. Another',
        help="Another type of house not contempleted on the options",
        states={
            'required': Eval('housing_type').in_(['another']),
            'readonly': ~Eval('housing_type').in_(['another'])
            })
    water = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('profundas', 'Aguas Profundas'),
        ('superficiales', 'Superficiales'),
        ('potable', 'Potable'),
        ], 'Agua',sort=False, required = True)
    water_location = fields.Selection([
        (None, ''),
        ('no_data','No data'),
        ('pipes_inside_house','Pipes inside the house'),
        ('outside_house_inside_land','Outside the house, but inside the land'),
        ('outside_land','Outside the land'),
        ],'Water location',sort=False,required=True)
    water_source = fields.Selection([
        (None,''),
        ('no_data','No data'),
        ('public_grid','Public network'),
        ('motor_pump','Drilling with motor pump'),
        ('manual pump','Drilling with manual pump'),
        ('well_no_pump','Drilling without pump'),
        ('other','Other'),
        ],'Water source', sort=False, required=True)
    water_source_other = fields.Char('Water source other',
        states={
            'required': Eval('water_source').in_(['other']),
            'readonly': ~Eval('water_source').in_(['other'])
            })
    bathroom = fields.Selection([
        (None,''),
        ('no_data','No data'),
        ('inside_house','Inside the house'),
        ('outside_house','Outside the house'),
        ('no','No'),
        ],'Bathroom',sort=False, required=True)
    bathroom_type = fields.Selection([
        (None,''),
        ('no_data','No data'),
        ('toilet_w_button','Toilet with button'),
        ('toilet_wo_button','Toilet without button'),
        ('letrine','Letrine'),
        ], 'Bathroom type',sort=False,
        states={
            'readonly': Eval('bathroom').in_(['no','no_data',None]),
            'required': Eval('bathroom').in_(['inside_house','outside_house']),
            })
    snow_ice = fields.Selection([
        (None,''),
        ('no_data','No data'),
        ('yes','Yes'),
        ('no','No'),
        ],'Snow or ice',sort=False, required=True,
        help="Snow or ice on the streets")
    excretes = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('abierto', 'Cielo Abierto'),
        ('letrina1', 'Letrina Sanitaria Buena'),
        ('letrina2', 'Letrina Sanitaria Regular'),
        ('letrina3', 'Letrina Sanitaria Mala'),
        ('cloacas', 'Cloacas'),
        ],'Excretas',sort=False, required=True)
    disposal = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('abierto', 'Cielo Abierto'),
        ('pozo', 'Pozo basurero con enterramiento'),
        ('quema', 'Quema'),
        ('recoleccion', 'Recoleccion Domicialiaria'),
        ],'Basuras',sort=False, required=True)
    walls = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('barro', 'Barro o Adobe'),
        ('ladrillo', 'Ladrillo'),
        ('chapa', 'Chapa'),
        ('carton', 'Carton o lona'),
        ('madera', 'Madera'),
        ('paja', 'Paja'),
        ('otro', 'Otros'),
        ],'Paredes',sort=False, required=True)
    walls_else = fields.Char('Otros',help='Caracteristica de las paredes no contempladas. Obligatorio',
                states={'invisible':Eval('walls')!='otro',
                        'required':Eval('walls')=='otro'})
    roofs = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('adobe', 'Barro o Adobe'),
        ('ladrillo', 'Ladrillo'),
        ('chapa', 'Chapa'),
        ('carton', 'Carton o lona'),
        ('madera', 'Madera'),
        ('paja', 'Paja'),
        ('otro', 'Otros'),
        ],'Techos',sort=False, required=True)
    roof_else = fields.Char('Otros',help='Caracteristica del techo no contemplada. Obligatorio',
                        states={'invisible':Eval('roofs')!='otro',
                                'required':Eval('roofs')=='otro'})
    floors = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('ceramico', 'Ceramico'),
        ('cemento', 'Cemento'),
        ('tierra', 'Tierra'),
        ('madera', 'Madera'),
        ('otros', 'Otros'),
        ],'Pisos',sort=False, required=True)
    floor_else = fields.Char('Otros',help='Caracteristica del piso no contemplada. Obligatorio',
        states={
            'invisible':~Eval('floors').in_(['otros']),
            'required':Eval('floors').in_(['otros'])
            })
    housemates = fields.Integer('Num. Habitantes', help='Cuantos habitantes viven'
        'en la misma unidad domiciliaria', required=True)
    minors = fields.Integer('Minors', help='How many minors are there in the house?',
        required=True)
    observations = fields.Text('Observaciones')
    gas = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('envasado', 'Gas envasado'),
        ('natural', 'Gas natural'),
        ('ninguno','Ninguno'),
        ],'Gas',sort=False, required=True)
    stove_feed = fields.Selection([
        (None,''),
        ('no_data','No data'),
        ('grid_gas','Gas of the grid'),
        ('tubed_gas','Gas tubed'),
        ('wood_coal','Wood or coal'),
        ('electricity','Electricity'),
        ('another','Another'),
        ],'Stove feed', required=True)
    kitchen = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('si', 'Si'),
        ('no', 'No'),
        ],'Cocina',help = 'Se encuentra el cuarto de cocina independiente',
        sort=False, required=True)
    kitchen_chars = fields.Selection([
        (None,''),
        ('no_data','No data'),
        ('water_and_drain','With water and drain instalation'),
        ('water_wo_drain','With water and without drain instalation'),
        ('wo_water','Without water instalation'),
        ],'Kitchen localization', sort=False,
        states={
            'readonly': Eval('kitchen').in_([None,'sin_dato','no']),
            'required': Eval('kitchen').in_(['si'])
            })
    electricity = fields.Selection([
        (None,''),
        ('sin_dato', 'Sin dato'),
        ('si', 'Si'),
        ('no', 'No'),
        ],'Electricidad', sort=False, required=True)
    bedrooms = fields.Integer('Dormitorios',help='Cantidad de habitaciones',required=True)
    malnutrition = fields.Boolean('Desnutridos')
    infant_death = fields.Boolean('Muertes Infantiles (+ de 2)')
    alcoholism = fields.Boolean('Alcoholismo')
    tbc = fields.Boolean('T.B.C.')
    drugs = fields.Boolean('Drogadiccion/Anorexia-Bulimia')
    vaccines_lack = fields.Boolean('Falta de Vacunas')
    disability = fields.Boolean('Discapacidad')
    std = fields.Boolean('I.T.S.')
    unemployed = fields.Boolean('Desocupado/T. inestable')
    children = fields.Boolean('+ de 5 ninos')
    single_parent = fields.Boolean('Madre o Padre solo')
    violence = fields.Boolean('Violencia Familiar')
    illiteracy = fields.Boolean('Analfabetismo')
    prostitution = fields.Boolean('Prostitucion')
    elderness = fields.Boolean('Ancianidad')
    no_id = fields.Boolean('N.N. sin DNI')
    teenage_pregnancy = fields.Boolean('Emb. Adolescente')
    unhealthy_housing = fields.Boolean('Medio Ambiente Insalubre')
    chagas = fields.Boolean('Chagas')
    overcrowding = fields.Boolean('Hacinamiento',help='Se considera\
                hacinamiento mas de 3 personas por\
                habitacion -- este campo lo completa el sistema')
    critical_housing = fields.Boolean('Vivienda Critica')
    threshold0 = fields.Function(fields.Integer('TH0'),'get_threshold0')
    threshold1 = fields.Function(fields.Integer('TH1'),'get_threshold1')
    threshold2 = fields.Function(fields.Integer('TH2'),'get_threshold2')
    count = fields.Integer('Marcas')

    def get_threshold0(self,name):
      return 0

    def get_threshold1(self,name):
      return 1

    def get_threshold2(self,name):
      return 2

    @staticmethod
    def default_bedrooms():
      return 1

    @fields.depends('housemates','bedrooms')
    def on_change_with_overcrowding(self):
        overcrowding = self.overcrowding
        if (self.bedrooms == 0):
            overcrowding = True
        elif self.housemates != None:
            if float(self.housemates)/float(self.bedrooms) > 3.0:
                overcrowding = True
            else:
                overcrowding = False
        return overcrowding

    @fields.depends('bedrooms','housemates','malnutrition','infant_death',
        'alcoholism','tbc','drugs','vaccines_lack','disability','std',
        'unemployed','children','single_parent','violence',
        'illiteracy','prostitution','elderness','no_id',
        'teenage_pregnancy', 'unhealthy_housing',
        'overcrowding','chagas')
    def on_change_with_count(self):
        count = 0
        if self.malnutrition==True:
            count=count+1
        if self.infant_death==True:
            count=count+1
        if self.alcoholism==True:
            count=count+1
        if self.tbc==True:
            count=count+1
        if self.drugs==True:
            count=count+1
        if self.vaccines_lack==True:
            count=count+1
        if self.disability==True:
            count=count+1
        if self.std==True:
            count=count+1
        if self.unemployed==True:
            count=count+1
        if self.children==True:
            count=count+1
        if self.single_parent==True:
            count=count+1
        if self.violence==True:
            count=count+1
        if self.illiteracy==True:
            count=count+1
        if self.prostitution==True:
            count=count+1
        if self.elderness==True:
            count=count+1
        if self.no_id==True:
            count=count+1
        if self.teenage_pregnancy==True:
            count=count+1
        #if self.poor_housing==True:
            #count=count+1
        if self.unhealthy_housing==True:
            count=count+1
        if self.overcrowding==True:
            count=count+1
        if self.chagas==True:
            count=count+1
        return count

    @fields.depends('count','bedrooms','housemates','malnutrition',
      'infant_death','alcoholism','tbc','drugs','vaccines_lack',
      'disability','std','unemployed','children','single_parent','violence',
      'illiteracy','prostitution','elderness','no_id','teenage_pregnancy',
      'unhealthy_housing','overcrowding','chagas')
    def on_change_with_critical_housing(self):
        if self.count not in range(0,3):
            critical_housing = True
        else:
            critical_housing = False
        return critical_housing
