.. image:: https://www.gnuhealth.org/downloads/artwork/logos/isologo-gnu-health.png 


GNU Health HMIS: Libre Hospital Management and Health Information System
========================================================================

GNU Health Housing Conditions FIUNER
------------------------------------
Este modulo permite:
  * Agregar un menu de configuration para seter valores por defecto para los campos ciudad, código postal, país, subdivision, en los registros de direcciones y unidades domiciliarias.
  * Separa la evaluación domiciliaria del registro de unidades domiciliarias.
  * Agrega un asistente para agrupar facilmente varias personas en una misma unidad domiciliaria.
  
This module add support to:
  * Adds a configuration menu to setup default values for city, zip, country, subdivision fields on addresses and domiciliary units.
  * Separates the domiciliary assessment of the domiciliary units.
  * Adds a wizard to easily regroup several persons into a same domiciliary unit.

Documentation
-------------
* GNU Health
Wikibooks: https://en.wikibooks.org/wiki/GNU_Health/

Contact
-------
* GNU Health Contact 

 - website: https://www.gnuhealth.org
 - email: info@gnuhealth.org
 - Twitter: @gnuhealth

* FIUNER Contact 

 - email: saludpublica@ingenieria.uner.edu.ar
 
*  Silix
  website: http://www.silix.com.ar
  email: contacto@silix.com.ar

License
--------

GNU Health is licensed under GPL v3+::

 Copyright (C) 2008-2021 Luis Falcon <falcon@gnuhealth.org>
 Copyright (C) 2011-2021 GNU Solidario <health@gnusolidario.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


Prerequisites
-------------

 * Python 3.6 or later (http://www.python.org/)
 * Tryton 6.0 (http://www.tryton.org/)
 * GNUHealth 4.0


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
