# -*- coding: utf-8 -*-
##############################################################################

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

class Address(metaclass = PoolMeta):
    "Address"
    __name__ = 'party.address'

    @staticmethod
    def default_city():
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config (1)
        if config.default_du_address_city:
            return config.default_du_address_city
        return None

    @staticmethod
    def default_zip():
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config (1)
        if config.default_du_address_zip:
            return config.default_du_address_zip
        return None

    @staticmethod
    def default_subdivision():
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config (1)
        if config.default_du_subdivision:
            return config.default_du_subdivision.id
        return None

    @staticmethod
    def default_country():
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config (1)
        if config.default_du_country:
            return config.default_du_country.id
        return None
