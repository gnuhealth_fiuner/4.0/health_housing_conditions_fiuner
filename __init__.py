from trytond.pool import Pool
from . import health_housing
from . import configuration
from . import health
from . import address
from .wizard import wizard_update_du

def register():
    Pool.register(
        health_housing.HousingConditions,
        health.DomiciliaryUnit,
        health.Party,
        address.Address,
        configuration.Configuration,
        configuration.ConfigurationDefault,
        configuration.GnuHealthSequences,
        wizard_update_du.DUDetected,
        wizard_update_du.WizardUpdateDUStart,
        wizard_update_du.WizardUpdateDUList,
        module='health_housing_conditions_fiuner', type_='model')
    Pool.register(
        wizard_update_du.WizardUpdateDU,
        module='health_housing_conditions_fiuner', type_='wizard')
