import urllib.parse

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Eval, Not, Bool, Equal, And, Greater, Or
from trytond import backend


class DomiciliaryUnit(metaclass = PoolMeta):
    'Domiciliary Unit'
    __name__ = 'gnuhealth.du'    

    du_code_autocomplete = fields.Boolean('Autocompletar codigo du',
                            help ='Tildar si quiere que se'
                                ' autocomplete el codigo domiciliario'
                                ' a medida que se llenan los campos')

    critical_housing = fields.Function(fields.Boolean('Vivienda Critica'),
                            'get_critical_housing',searcher='search_critical_housing')
    count = fields.Function(fields.Integer('Marcas'),'get_count')
    threshold0 = fields.Function(fields.Integer('TH0'),'get_threshold0')
    threshold1 = fields.Function(fields.Integer('TH1'),'get_threshold1')
    threshold2 = fields.Function(fields.Integer('TH2'),'get_threshold2')
    urladdr2 = fields.Char(
        'Google map',
        help="Locates the DU on the Google Map by default")

    def get_critical_housing(self,name):
      pool = Pool()
      Housing_Conditions = pool.get('gnuhealth.housing')
      about_this_du = Housing_Conditions.search(['du','=',self.id])
      if len(about_this_du)!=0:
          return about_this_du[-1].critical_housing
      return None

    @classmethod
    def search_critical_housing(cls, name, clause):
         cursor = Transaction().connection.cursor()
         pool = Pool()
         DomiciliaryUnit = pool.get('gnuhealth.du')
         HousingConditions = pool.get('gnuhealth.housing')
         cursor.execute('SELECT du.id '
             'FROM "' + DomiciliaryUnit._table + '" du '
                 'INNER JOIN ( '
                     'SELECT DISTINCT ON (du) du, critical_housing '
                     'FROM "' + HousingConditions._table + '" '
                     'ORDER BY du, id DESC '
                 ') last_hc '
                 'ON du.id = last_hc.du '
             'WHERE last_hc.critical_housing = TRUE')
         has_critical_housing = cursor.fetchall()
         field, op, operand = clause
         if (op, operand) in (('=', True), ('!=', False)):
             return [('id', 'in', [x[0] for x in has_critical_housing])]
         elif (op, operand) in (('=', False), ('!=', True)):
             return [('id', 'not in', [x[0] for x in has_critical_housing])]
         else:
             return []

    def get_count(self,name):
      pool = Pool()
      Housing_Conditions = pool.get('gnuhealth.housing')
      about_this_du = Housing_Conditions.search(['du','=',self.id])
      if len(about_this_du)!=0:
          return about_this_du[-1].count
      return 0

    def get_threshold0(self,name):
      return 0

    def get_threshold1(self,name):
      return 1

    def get_threshold2(self,name):
      return 2

    @staticmethod
    def default_address_city():
      Config = Pool().get('gnuhealth.du.configuration')
      config = Config(1)
      if config.default_du_address_city:
          return config.default_du_address_city
      return None

    @staticmethod
    def default_address_zip():
      Config = Pool().get('gnuhealth.du.configuration')
      config = Config(1)
      if config.default_du_address_zip:
          return config.default_du_address_zip
      return None

    @staticmethod
    def default_address_country():
      Config = Pool().get('gnuhealth.du.configuration')
      config = Config(1)
      if config.default_du_country:
          return config.default_du_country.id
      return None

    @staticmethod
    def default_address_subdivision():
      Config = Pool().get('gnuhealth.du.configuration')
      config = Config(1)
      if config.default_du_subdivision:
          return config.default_du_subdivision.id
      return None

    @staticmethod
    def default_du_code_autocomplete():
      return True 

    @fields.depends('du_code_autocomplete','address_street',
                    'address_street_number','address_street_bis',
                    'address_district', 'address_municipality',
                    'address_city','address_subdivision')
    def on_change_with_name(self):
       if(self.du_code_autocomplete):
        municipality = self.address_municipality or ''
        city = self.address_city or ''
        district = self.address_district or ''           
        if (self.address_street or str(self.address_street_number) or self.address_street_bis 
        or self.address_district
        or self.address_municipality or self.address_city or self.address_subdivision):
            address_street_number = str(self.address_street_number or '')
            address_street_bis = self.address_street_bis or ''
            res = (self.address_street or '')+' '+address_street_number+' '
            res += address_street_bis+' '+municipality +' '+city+' '+district
            res = res.replace('  ',' ')
            return res
       return None

    def get_du_address(self, name):
        old = super(DomiciliaryUnit,self).get_du_address(name)
        du_addr=''
        # Street
        if (self.address_street):
            du_addr = str(self.address_street or '') + ' ' + \
                str(self.address_street_number) + "\n"            
       # City
        if (self.address_city):
            du_addr +=self.address_city        
        # Zip Code
        if (self.address_zip):
            du_addr = du_addr +" - "+ self.address_zip +'\n'            
        # Grab the parent subdivisions
        if (self.address_subdivision):
            du_addr = du_addr + \
                str(self.get_parent(subdivision=self.address_subdivision))
        # Country
        if (self.address_country):
            du_addr = du_addr +"\n"+ self.address_country.rec_name
        return du_addr

    @fields.depends('address_street',
        'address_street_number',
        'address_city', 'address_subdivision',
        'address_country')
    def on_change_with_urladdr2(self):
        # Generates the URL to be used in Google Map
        #   If latitude and longitude are known, use them.
        #   Otherwise, use street, municipality, city, and so on.
        lang = Transaction().language[:2]
        url = ' '.join([
            self.address_street or '',str(self.address_street_number or ''),
            self.address_city or '',
            self.address_subdivision and self.address_subdivision.name or '',
            self.address_country and self.address_country.name or ''])
        if url.strip():
            return 'http://maps.google.com/maps?hl=%s&q=%s' % \
                (lang, urllib.parse.quote(url))
        return ''

    @classmethod
    def __register__(cls, module_name):
        super(DomiciliaryUnit, cls).__register__(module_name)
        pool = Pool()
        DU = pool.get('gnuhealth.du')
        Housings = pool.get('gnuhealth.housing')
        Date = pool.get('ir.date')

        #TableHandler = backend.get('TableHandler')
        #table = TableHandler(cls, module_name)
        cursor = Transaction().connection.cursor()

        DU_table = backend.TableHandler(DU, module_name)

        housing = Housings.search([('id','>','0')])

        if DU_table.column_exist('agua') and not housing:
            #select all du's that has any data about housing conditions
            du = DU.search(['OR',
                         ('agua','!=',None),
                         ('excretas','!=',None),
                         ('basuras','!=',None),
                         ('paredes','!=',None),
                         ])
            #If there is no housing, we create them and migrate the data from
            #du's z_DU 3.8 version fields
            if du:
                housing_to_create = [{
                        'du': x.id,
                        'revision_date': Date.today(),
                        'water': x.agua or 'sin_dato',
                        'excretes': x.excretas or 'sin_dato',
                        'disposal': x.basuras or 'sin_dato',
                        'walls': x.paredes or 'sin_dato',
                        'walls_else': x.paredes_otros,
                        'roofs': x.techos or 'sin_dato',
                        'roof_else': x.techos_otros,
                        'floors': x.pisos or 'sin_dato',
                        'floor_else': x.pisos_otros,
                        'housemates': x.habitantes,
                        'observations': x.observaciones,
                        'gas': x.gas1 or 'sin_dato',
                        'kitchen': x.cocina or 'sin_dato',
                        'housing_type': x.tipo_vivienda or 'sin_dato',
                        'electricity': x.electricidad or 'sin_dato',
                        'bedrooms': x.habitaciones or 0,
                        'malnutrition': x.desnutridos,
                        'infant_death': x.muertes_infantiles,
                        'alcoholism': x.alcoholismo,
                        'tbc': x.tbc,
                        'drugs': x.drogas,
                        'vaccines_lack': x.vacunas,
                        'disability': x.discapacidad,
                        'std': x.its,
                        'children': x.ninos,
                        'single_parent': x.madre_padre_solo,
                        'violence': x.violencia,
                        'illiteracy': x.analfabetismo,
                        'prostitution': x.prostitucion,
                        'elderness': x.ancianidad,
                        'no_id': x.sin_dni,
                        'teenage_pregnancy': x.embarazo_adolescente,
                        'unhealthy_housing': x.medio_ambiente,
                        'chagas': x.chagas,
                        'overcrowding': x.hacinamiento,
                        'critical_housing': x.vivienda_critica,
                        'count': x.cont,
                        } for x in du]
                Housings.create(housing_to_create)
        #If there is no housing, we create them and migrate the data from
        #du's original health du fields
        if DU_table.column_exist('water') and not housing:
            #select all du's that has any data about housing conditions
            du = DU.search(['OR',
                         ('bathrooms','!=',None),
                         ('roof_type','!=',None),
                         ('sewers','!=',None),
                         ('dwelling','!=',None),
                         ('electricity','!=',None),
                         ('housing','!=',None),
                         ('materials','!=',None),
                         ('bedrooms','!=',None),
                         ('gas','!=',None),
                         ('water','!=',None),
                         ('trash','!=',None),
                         ])
            #If there is no housing, we create them and migrate the data from
            #du's
            if du:
                housing_to_create = [{
                        'du': x.id,
                        'revision_date': Date.today(),
                        'water': 'potable' if x.water else 'sin_dato',
                        'excretes': 'cloacas' if x.sewers else 'sin_dato',
                        'disposal':'recoleccion' if x.trash else 'sin_dato',
                        'walls': 'barro' if ((x.materials == 'mud')or(x.materials == 'adobe')) else
                                    'madera' if x.materials == 'wood' else
                                    'ladrillo' if x.materials == 'concrete' else
                                    'otro' if((x.materials == 'adobe')or(x.materials == 'stone'))
                                    else 'sin_dato',
                        'walls_else': 'piedra' if x.materials == 'stone'
                                        else '',
                        'roofs': 'adobe' if ((x.materials == 'mud') or(x.materials == 'adobe' )) else
                                    'madera' if x.materials == 'wood' else
                                    'ladrillo' if x.materials == 'concrete' else
                                    'otro' if((x.materials == 'adobe')or(x.materials == 'stone'))
                                    else 'sin_dato',
                        'roof_else': 'piedra' if x.materials == 'stone' else
                                        'paja' if x.materials == 'thatch'
                                        else '',
                        'housemates': '1',
                        'floors': 'sin_dato',
                        'observations': x.desc or '',
                        'gas': 'sin_dato' if x.gas else 'ninguno',
                        'kitchen': 'sin_dato',
                        'housing_type': '3' if x.housing == '0' else
                                        '2' if ((x.housing == '1' ) or
                                                (x.housing == '2')) else
                                        '1' if ((x.housing == '3') or
                                                (x.housing == '4')) else
                                        'sin_dato',
                        'electricity': 'si' if x.electricity else
                                        'sin_dato',
                        'bedrooms': x.bedrooms or 0,
                        } for x in du]
                Housings.create(housing_to_create)
        ###drop all column migrated that won't be used anymore
        if DU_table.column_exist('agua'):
            DU_table.drop_column('agua')

        if DU_table.column_exist('excretas'):
            DU_table.drop_column('excretas')

        if DU_table.column_exist('basuras'):
            DU_table.drop_column('basuras')

        if DU_table.column_exist('paredes'):
            DU_table.drop_column('paredes')

        if DU_table.column_exist('paredes_otros'):
            DU_table.drop_column('paredes_otros')

        if DU_table.column_exist('techos'):
            DU_table.drop_column('techos')

        if DU_table.column_exist('techos_otros'):
            DU_table.drop_column('techos_otros')

        if DU_table.column_exist('pisos'):
            DU_table.drop_column('pisos')

        if DU_table.column_exist('pisos_otros'):
            DU_table.drop_column('pisos_otros')

        if DU_table.column_exist('habitantes'):
            DU_table.drop_column('habitantes')

        if DU_table.column_exist('gas1'):
            DU_table.drop_column('gas1')

        if DU_table.column_exist('cocina'):
            DU_table.drop_column('cocina')

        if DU_table.column_exist('tipo_vivienda'):
            DU_table.drop_column('tipo_vivienda')

        if DU_table.column_exist('electricidad'):
            DU_table.drop_column('electricidad')

        if DU_table.column_exist('habitaciones'):
            DU_table.drop_column('habitaciones')

        if DU_table.column_exist('desnutridos'):
            DU_table.drop_column('desnutridos')

        if DU_table.column_exist('muertes_infantiles'):
            DU_table.drop_column('muertes_infantiles')

        if DU_table.column_exist('alcoholismo'):
            DU_table.drop_column('alcoholismo')

        if DU_table.column_exist('tbc'):
            DU_table.drop_column('tbc')

        if DU_table.column_exist('drogas'):
            DU_table.drop_column('drogas')

        if DU_table.column_exist('vacunas'):
            DU_table.drop_column('vacunas')

        if DU_table.column_exist('discapacidad'):
            DU_table.drop_column('discapacidad')

        if DU_table.column_exist('its'):
            DU_table.drop_column('its')

        if DU_table.column_exist('desocupado'):
            DU_table.drop_column('desocupado')

        if DU_table.column_exist('ninos'):
            DU_table.drop_column('ninos')

        if DU_table.column_exist('madre_padre_solo'):
            DU_table.drop_column('madre_padre_solo')

        if DU_table.column_exist('violencia'):
            DU_table.drop_column('violencia')

        if DU_table.column_exist('analfabetismo'):
            DU_table.drop_column('analfabetismo')

        if DU_table.column_exist('prostitucion'):
            DU_table.drop_column('prostitucion')

        if DU_table.column_exist('ancianidad'):
            DU_table.drop_column('ancianidad')

        if DU_table.column_exist('sin_dni'):
            DU_table.drop_column('sin_dni')

        if DU_table.column_exist('embarazo_adolescente'):
            DU_table.drop_column('embarazo_adolescente')

        if DU_table.column_exist('vivienda'):
            DU_table.drop_column('vivienda')

        if DU_table.column_exist('medio_ambiente'):
            DU_table.drop_column('medio_ambiente')

        if DU_table.column_exist('chagas'):
            DU_table.drop_column('chagas')

        if DU_table.column_exist('hacinamiento'):
            DU_table.drop_column('hacinamiento')

        if DU_table.column_exist('vivienda_critica'):
            DU_table.drop_column('vivienda_critica')

        if DU_table.column_exist('cont'):
            DU_table.drop_column('cont')

        if DU_table.column_exist('umbral2'):
            DU_table.drop_column('umbral2')

        if DU_table.column_exist('umbral1'):
            DU_table.drop_column('umbral1')

        if DU_table.column_exist('umbral0'):
            DU_table.drop_column('umbral0')

        if DU_table.column_exist('observaciones'):
            DU_table.drop_column('observaciones')


class Party(metaclass = PoolMeta):
    'Party'
    __name__ = 'party.party'

    @staticmethod
    def default_citizenship():
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config(1)
        if config.default_citizenship:
            return config.default_citizenship.id
        return None

    @staticmethod
    def default_residence():
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config(1)
        if config.default_residence:
            return config.default_residence.id
        return None
