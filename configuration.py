from trytond import backend
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.model import MultiValueMixin, ValueMixin
from trytond.pool import Pool
from trytond.tools.multivalue import migrate_property
from trytond.pyson import Eval, Bool


default_du_address_city = fields.Char("Address city", help="Default address city")
default_du_country = fields.Many2One('country.country',
                "Default Domicialiary Unit Country",
                required=True)
default_du_subdivision = fields.Many2One('country.subdivision',
                "Default Domicialiary Unit Subdivision",
                domain=[('country','=',Eval('default_du_country'))],
                depends=['default_du_country'],
                required=True)
default_du_address_zip = fields.Char('Codigo Postal')
default_citizenship = fields.Many2One('country.country',
                "Default nationality",
                required=True)
default_residence = fields.Many2One('country.country',
                "Default country",
                required=True)


class Configuration(ModelSingleton, ModelSQL, ModelView, MultiValueMixin):
    'Domiciliary Unit and Citizenship Configuration'
    __name__ = 'gnuhealth.du.configuration'

    default_du_address_city = default_du_address_city
    default_du_address_zip = default_du_address_zip
    default_du_country = default_du_country
    default_du_subdivision = default_du_subdivision
    default_citizenship = default_citizenship
    default_residence = default_residence


class ConfigurationDefault(ModelSQL, ValueMixin):
    "GnuHealth Default Configuration"
    __name__ = 'gnuhealth.du.configuration.default'

class GnuHealthSequences(ModelSingleton, ModelSQL, ModelView):
    "GNU Healt DU Sequences"
    __name__ = 'gnuhealth.du.sequences'

    housing_du_conditions_survey_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Housing DU Conditions Survey Sequence', required=True,
        domain=[('code', '=', 'gnuhealth.housing')]))
