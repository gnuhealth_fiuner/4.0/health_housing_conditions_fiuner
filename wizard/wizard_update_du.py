#This file is part health_housing_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from datetime import datetime, date

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, Button
from trytond.pool import Pool
from trytond.pyson import Eval, Equal, Bool, Not


class DUDetected(ModelView):
    'DU Detected'
    __name__ = 'gnuhealth.detected.du'

    du_code = fields.Char('DU code')
    du = fields.Text('DU')    
    members = fields.Text('Members')    
    to_delete = fields.Boolean('Delete DU?')

    @staticmethod
    def default_to_delete():
        return False


class WizardUpdateDUStart(ModelView):
    'Batch Patient Census Data Start'
    __name__ = 'gnuhealth.wizard.update.du.start'

    choose_origin = fields.Selection([
        ('familiar_group','Familiar group'),
        ('patient','Patient'),
        ],'Choose',sort=False,required=True)    
    patient = fields.Many2Many(
        'gnuhealth.patient',
        None,None,'Pacientes',
        states = 
            {
            'required': Equal(Eval('choose'),'patient'),
            'invisible': Not(Bool(Equal(Eval('choose_origin'),'patient'))),
            })
    familiar_group = fields.Many2One(
        'gnuhealth.family','Family',
        states = 
            {
            'required': Equal(Eval('choose'),'patient'),
            'invisible': Not(Bool(Equal(Eval('choose_origin'),'familiar_group'))),
            })

    @staticmethod
    def default_choose_origin():
        return 'patient'


class WizardUpdateDUList(ModelView):
    'Batch Patient Census Prevalidation'
    __name__ = 'gnuhealth.wizard.update.du.list'

    du_list_detected = fields.One2Many(
        'gnuhealth.detected.du',None,
        'DU List',readonly=True)
    du_selected = fields.Many2One(
        'gnuhealth.du','DU Selected',
        required=True,
        domain = 
            [('id','in', Eval('du_selected_domain'))]
            )
    du_selected_domain = fields.Function(
        fields.Many2Many('gnuhealth.du',None,None,'DU selected domain'),
        'on_change_with_du_selected_domain')

    @fields.depends('du_list_detected')
    def on_change_with_du_selected_domain(self, name=None):
        DU = Pool().get('gnuhealth.du')
        du_list = DU.search([
            ('name','in',[x.du_code for x in self.du_list_detected])
            ])
        return [x.id for x in du_list]


class WizardUpdateDU(Wizard):
    'Batch Patient Census Data'
    __name__ = 'gnuhealth.wizard.update.du'

    start_state = 'start'

    start = StateView(
        'gnuhealth.wizard.update.du.start',
        'health_housing_conditions_fiuner.gnuhealth_wizard_update_du_start_view', [
            Button('Ok', 'select_du', 'tryton-ok', default=True),
            Button('Cancel', 'end', 'tryton-cancel'),            
        ])

    select_du = StateTransition()

    du_list_data = StateView(
        'gnuhealth.wizard.update.du.list',
        'health_housing_conditions_fiuner.gnuhealth_wizard_update_du_list_view',[
            Button('Update','update','tryton-ok'),
            Button('Update and restart','update_and_restart','tryton-back'),
            Button('Cancel','end','tryton-cancel')
            ])

    update = StateAction('health.gnuhealth_action_du')

    update_and_restart = StateAction('health_housing_conditions_fiuner.act_update_du_wizard')

    def transition_select_du(self):
        return 'du_list_data'

    def default_du_list_data(self,fields):
        def du_members(members):
            return '\n'.join([x.name+', '+x.lastname for x in members])

        Party = Pool().get('party.party')
        DU = Pool().get('gnuhealth.du')

        party_list = []
        party_list = list(set([x.patient.name.id for x in self.start.familiar_group]))\
            if self.start.choose_origin == 'familiar_group'\
                else list(set([x.name.id for x in self.start.patient]))
        self.update.party_list = party_list

        party = Party.search([('id','in',party_list)])
        du = DU.search([('id','in',[x.du.id for x in party if x.du])])
        du_list = [{
                    'du_code': x.name,
                    'du': x.address_repr,
                    'members': du_members(x.members),
                    } for x in du]
        return {
            'du_list_detected': du_list,
            }

    def update_func(self,action):
        Party = Pool().get('party.party')
        DU = Pool().get('gnuhealth.du')

        party_list = Party.search([('id','in',self.update.party_list)])

        du_list = self.du_list_data.du_selected_domain
        du_selected = self.du_list_data.du_selected

        for party in party_list:
            party.du = du_selected.id
            party.save()

        data = {'res_id': [x.id for x in [du_selected]]}
        return action, data

    def do_update(self, action):
        action, data = self.update_func(action)
        action['views'].reverse()
        return action, data

    def do_update_and_restart(self, action):
        action, data = self.update_func(action)
        return action, data
